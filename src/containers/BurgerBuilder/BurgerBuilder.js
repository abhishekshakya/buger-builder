import React , {Component} from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'

const INGREDIENT_PRICE = {
    salad: 0.5 ,
    bacon: 0.4,
    chees: 1.3,
    meat : 0.7,
}


class BurgerBuilder extends Component {
    // constructor(props){
    //      super(props);
    //      this.state = {

    //      }
    // }
    state = {
        ingredients : {
            salad : 0,
            bacon : 0,
            cheese : 0,
            meat : 0,
        },
         totalPrice : 4,
         purchasable :false
    }
    updatePurchasable () {
        const ingredients = {
            ...this.state.ingredients
        };
        const sum = Object.keys(ingredients)
        .map( igKey => {
            return ingredients[igKey];
        })
        .reduce((sum , el) => {
            return sum+el;
        }, 0);
        this.setState({purchasable: sum > 0 });
    }
    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        if(oldCount <= 0){
            return;
        }
        const updateCount = oldCount - 1;
        const updateIngredients = {
            ...this.state.ingredients
        };
        updateIngredients[type] = updateCount;
        const priceDedction = INGREDIENT_PRICE[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDedction ;
        this.setState({totalPrice:newPrice , ingredients : updateIngredients})

    }
    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updateCount = oldCount + 1;
        const updateIngredients = {
            ...this.state.ingredients
        };
        updateIngredients[type] = updateCount;
        const priceAddition = INGREDIENT_PRICE[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition ;
        this.setState({totalPrice:newPrice , ingredients : updateIngredients})

    }
    render(){

        return (
            <Aux>
               <Burger ingredients={this.state.ingredients}/>
                <BuildControls ingredientAdded = {this.addIngredientHandler}
                ingredientRemove = {this.removeIngredientHandler}
                price ={this.state.totalPrice}
                />
            </Aux>
        )
    }
}

export default BurgerBuilder;