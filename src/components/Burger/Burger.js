import React from 'react';
import classes from './Burger.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'
const burger = (props) => {
    let transformIngrediens = Object.keys(props.ingredients)
    .map(igKey => {
        return [...Array(props.ingredients[igKey])].map((_ , i) => {
            return <BurgerIngredient key = {igKey+i} type = {igKey} />
        }) 
    })
    .reduce((arr , el) => {
        return arr.concat(el);
    }, []);
    // console.log(transformIngrediens.length);
    if(transformIngrediens.length === 0){
           transformIngrediens = <p> Please start the adding ingredients</p>
    }

    return (
        <div className={classes.Burger}>
             <BurgerIngredient type="bread-top" /> 
                {transformIngrediens}
             <BurgerIngredient type="bread-bottom" /> 
             

        </div>
    )
}

export default burger;