import React from 'react';
import BuildControl from './BuildControl/BuildControl'
import classes from './BuildControls.css'


const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' },
];
// console.log(controls);

const buildControls = (props) => (
    <div className={classes.BuildControl}>
        <p> Current price : <strong>{props.price}</strong></p>
        {controls.map((ctrl) => (
            <BuildControl 
            key={ctrl.label} 
            lable={ctrl.label} 
            added = {() => props.ingredientAdded(ctrl.type)}
            removed = {() => props.ingredientRemove(ctrl.type)}
            
            />
        ))}
        <button className={classes.OrderButton}> ORDER NOW</button>
    </div>
)

export default buildControls;