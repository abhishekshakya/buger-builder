import React from  'react';
import Aux from '../../hoc/Aux';
import classes from './Layout.css'
const layout = (props) => (
    <Aux>
    <div>Toolbar , SideDrawer, BackDrop </div>
    <main className={classes.Content}>
        {props.children}
    </main>
    </Aux>
);

export default layout;

// removeIngredientHandler = (type) => {
//     const oldCount = this.state.ingredients[type];
//     const updateCount = oldCount - 1;
//     const updateIngredients = {
//         ...this.state.ingredients
//     };
//     updateIngredients[type] = updateCount;
//     const priceAddition = INGREDIENT_PRICE[type];
//     const oldPrice = this.state.totalPrice;
//     const newPrice = oldPrice - priceAddition ;
//     this.setState({totalPrice:newPrice , ingredients : updateIngredients})

// }